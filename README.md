**MAP4CNS**
===========
Map reads and perform DAG-based consensus using distributed computing.

DESCRIPTION
===========
In contrast to long-read assemblers employing the more-traditional strategy of consensus-based read-read error correction prior to OLC assembly, a number of state-of-the-art long-read *de novo* genome assemblers (such as [DBG2OLC](https://github.com/yechengxi/DBG2OLC) and [MINIASM](https://github.com/lh3/miniasm])) assemble long, noisy reads directly. The resulting backbone contigs maintain a 15% error rate and require "polishing" to remove these errors and other large sequencing artifacts.

`map4cns` is a Bash script coordinating the distributed parallelization of [pbdagcon](https://github.com/PacificBiosciences/pbdagcon) over an SGE/UGE computing grid to polish backbone sequences. `map4cns` takes as input an FOFN file of long-read FASTA files and a backbone genome sequence (also in FASTA format). First, the raw long-read data are aligned to the input backbone sequence, generating a single BAM file. The backbone sequences are virtually balanced into a user-defined number of bins and then `pbdagcon` is run on each backbone sequence independently on the grid. Depth of coverage is also computed. Finally, these distributed files are then merged into single output consensus FASTA and raw-read depth files.

This pipeline also provides a mechanism for running jobs in parallel on a single many-core machine. `map4cns` uses touch files to track the progression of the pipeline, so only one instance of `map4cns` should be run within a project directory at one time. 

USAGE
=====
```sh
map4cns [--reads-fofn <reads.fofn>] [--backbone-fasta <backbone.fasta>]
        [--prefix <out_prefix>] [--paired] [--collated] [--blasr]
        [--help] [-h]
```

INSTALL
=======
Dependencies
------------
`map4cns` has the following external dependencies (which have their own dependencies):

- **gcc** v4.9+ (required by some tools below)
- **Git** v1.8+
- **Perl** v5.10+
- **Python** v2.7+ (requires pysam)
- **SAMtools** v1.0+
- **BEDtools**
- **BWA** v0.7+
- **Blasr** v5.1+ (optional, but recommended)
- **pbdagcon**
- **SGE** (or similar; optional, but recommended)
- **Make**

Once all dependencies have been properly installed, all source paths must be accessible via the `$PATH` environment variable.  

Installation
------------
Installing `map4cns` requires an internet connection and Git. Issue the following commands on the commandline:
```bash
git clone git@bitbucket.org:rokhsar-lab/map4cns.git
cd map4cns
make
```
This will download and install some libraries into a `lib` subdirectory within the `map4cns` directory. Now you are good-to-go.

OPTIONS
=======
Command line options
-------------------
Coming soon.

Configuration file
------------------
`map4cns` uses a configuration file to parameterize the various tools coordinated by the pipeline and to request computing resources. If the user does not create the config file explicitly, the pipeline generates it. This config file *must* be named `map4cns.config` and be located within the run's working directory (set with `--prefix`). It is recommended to first create the working directory and place the config file within it before execution of the pipeline. An example config file is in [`example/map4cns.config`](example/map4cns.config).

BUGS & LIMITATIONS
==================
Known Issues
------------
The code for interacting with a cluster scheduler has been configured to run in the NERSC computing environment and has not been tested on other systems.

Some difficulty is known running in non-Bash shell environments.

Reporting
---------
All feature requests and bug reports should be directed to the author.

AUTHOR
=======
Jessen Bredeson <jessenbredeson@berkeley.edu>

COPYRIGHT
=========
Copyright (c)2013. The Regents of the University of California (Regents). All Rights Reserved. Permission to use, copy, modify, and distribute this software and its documentation for educational, research, and not-for-profit purposes, without fee and without a signed licensing agreement, is hereby granted, provided that the above copyright notice, this paragraph and the following two paragraphs appear in all copies, modifications, and distributions. Contact The Office of Technology Licensing, UC Berkeley, 2150 Shattuck Avenue, Suite 510, Berkeley, CA 94720-1620, (510) 643-7201, for commercial licensing opportunities.

Created by Jessen Bredeson, Department of Molecular and Cell Biology,
University of California, Berkeley.

IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE

REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
