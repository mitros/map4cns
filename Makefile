PRJ_DIR = $(PWD)
LIB_DIR = $(PRJ_DIR)/lib
SUB_DIR = $(PRJ_DIR)/submodules

all: modules

modules: mklib valkyr

mklib:
	mkdir -p $(LIB_DIR); chmod 755 $(LIB_DIR)

valkyr: mklib
	mkdir -p $(LIB_DIR)
	git submodule update --init --recursive $(SUB_DIR)/Valkyr
	$(shell cd $(SUB_DIR); find Valkyr -type d -exec mkdir -p $(LIB_DIR)/{} \;)
	$(shell cd $(SUB_DIR); find Valkyr -name "*.pm" -exec cp {} $(LIB_DIR)/{} \;)

clean:
	-rm -rf $(LIB_DIR)

